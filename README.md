# Description
House odd fish with odd characteristics.

# Building
## Required libs
- [SDL2](https://www.libsdl.org/download-2.0.php)
- [glm](https://glm.g-truc.net/0.9.8/index.html)
- [GLEW](http://glew.sourceforge.net/)

## Useful
- [cmake](https://cmake.org/)
- [git](https://git-scm.com/)

## Make
`mkdir build ; cd build/ ; cmake ../ && make`
