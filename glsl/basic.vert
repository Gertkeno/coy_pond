#version 330
#extension GL_ARB_explicit_uniform_location : enable
layout (location=0) in vec3 position;
layout (location=1) in vec2 texcoord;

layout (location=5) uniform mat4 model;

out vec2 TexCoord;

void main()
{
	TexCoord = texcoord;
	gl_Position = model * vec4 (position, 1.0);
}
