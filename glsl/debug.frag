#version 330
#extension GL_ARB_explicit_uniform_location : enable

in vec2 TexCoord;

layout (location=0) out vec3 outputDraw;

const float THRESHOLD = 0.02;
void main()
{
	bool above_min = min (TexCoord.x, TexCoord.y) > THRESHOLD;
	bool below_max = max (TexCoord.x, TexCoord.y) < 1.0 - THRESHOLD;
	if (above_min && below_max)
		discard;

	outputDraw = vec3(TexCoord, 1.0);
}
