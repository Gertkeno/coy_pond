#version 330
#extension GL_ARB_explicit_uniform_location : enable

in vec2 TexCoord;

layout (location=6) uniform vec3 colorMod;
layout (location=7) uniform float hue;
layout (location=20) uniform sampler2D tex0;
layout (location=21) uniform sampler2D tex1;
layout (location=0) out vec3 outputDraw;


vec3 hue_adjust (vec3 color, float adjust)
{
	if ((color.r == color.g && color.g == color.b) || adjust == 0.0)
		return color;

	// https://gist.github.com/mairod/a75e7b44f68110e1576d77419d608786
	const vec3 kRGBToYPrime = vec3 (0.299, 0.587, 0.114);
	const vec3 kRGBToI      = vec3 (0.596, -0.275, -0.321);
	const vec3 kRGBToQ      = vec3 (0.212, -0.523, 0.311);

	const vec3 kYIQToR = vec3 (1.0, 0.956, 0.621);
	const vec3 kYIQToG = vec3 (1.0, -0.272, -0.647);
	const vec3 kYIQToB = vec3 (1.0, -1.107, 1.704);

	float YPrime = dot (color, kRGBToYPrime);
	float I      = dot (color, kRGBToI);
	float Q      = dot (color, kRGBToQ);
	float hue    = atan (Q, I);
	float chroma = sqrt (I * I + Q * Q);

	hue += adjust;

	Q = chroma * sin (hue);
	I = chroma * cos (hue);

	vec3 yIQ = vec3 (YPrime, I, Q);

	return vec3 (dot (yIQ, kYIQToR), dot (yIQ, kYIQToG), dot (yIQ, kYIQToB));
}

void main()
{
	vec3 texData = texture2D (tex0, TexCoord).rgb;
	if (texData == vec3 (1.0, 0.0, 1.0))
		discard;

	//outputDraw = mix (texData, vec3 (TexCoord, 0.0), 0.7); // * colorMod;//
	outputDraw = hue_adjust (texData, hue) * colorMod;
}
