#pragma once
#include <SDL2/SDL_events.h>
#include <array>
#include "Window.hpp"

class StateBase;

class GameManager
{
	StateBase * _state;
	Window _window;
	SDL_Event * const _event;

	void _give_window() const;
public:
	GameManager(StateBase *);
	~GameManager();

	bool update();
};
