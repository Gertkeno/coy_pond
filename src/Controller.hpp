#pragma once
#include <glm/glm.hpp>
#include <SDL2/SDL_keycode.h>

union SDL_Event;

namespace controller
{
	void update();
	void events (SDL_Event *);

	glm::vec2 get_mouse();
	bool mouse_click(int right);
	bool mouse_held(int right);
	void update_window (int w, int h);

	bool is_pressed (int keycode);
	bool is_held (int keycode);
}
