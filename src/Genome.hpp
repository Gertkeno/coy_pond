#pragma once
#include <type_traits>

// behavioral and aesthetic changes for all
enum class gene_t: unsigned long
{
	NONE     = 0,
	LONG     = 1 << 0,
	ELECTRIC = 1 << 1,
	PUFFER   = 1 << 2,
	RAINBOW  = 1 << 3,
	SHADOW   = 1 << 4,
};

constexpr gene_t operator | (gene_t lhs, gene_t rhs)
{
	//using utype = typename std::underlying_type<gene_t>::type;
	using utype = typename std::underlying_type <gene_t>::type;
	const auto l {static_cast <utype> (lhs)};
	const auto h {static_cast <utype> (rhs)};
	return static_cast <gene_t> (l bitor h);
}

constexpr gene_t operator & (gene_t lhs, gene_t rhs)
{
	using utype = typename std::underlying_type <gene_t>::type;
	const auto l {static_cast <utype> (lhs)};
	const auto h {static_cast <utype> (rhs)};
	return static_cast <gene_t> (l bitand h);
}

constexpr bool operator == (gene_t lhs, unsigned long rhs)
{
	return static_cast <unsigned long> (lhs) == rhs;
}

constexpr bool operator != (gene_t lhs, unsigned long rhs)
{
	return not (lhs == rhs);
}
