#pragma once

class Texture
{
	unsigned _textureIndex;
public:
	Texture (const char * fn);
	~Texture();

	void enable (int texNum = 0) const;
};
