#pragma once
#include "../Node.hpp"

class StateBase
{
protected:
	struct AR
	{
		bool update;
		float w, h;
	} static _aspectRatio;
public:
	virtual ~StateBase(){};
	virtual StateBase * update() = 0;
	virtual void draw() = 0;

	static void update_aspect (float width, float height);
};
