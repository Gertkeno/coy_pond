#pragma once
#include "StateBase.hpp"

class Fish;

class TankView: public StateBase
{
	static Node _root;
	static glm::vec3 _cameraPos;

	static void aspect_update();
public:
	TankView();
	~TankView();

	StateBase * update();
	void draw();

	static Node * get_root() {return &_root;}
};
