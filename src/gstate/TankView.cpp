#include "TankView.hpp"

#include "../Fish.hpp"
#include "../DebugOut.hpp"
#include "../PondManager.hpp"
#include "../Controller.hpp"
#include "../FrameTime.hpp"

#include <glm/gtc/matrix_transform.hpp>

static constexpr float ZOOM_FACTOR {0.3f};

Node TankView::_root;
glm::vec3 TankView::_cameraPos;

void TankView::aspect_update()
{
	const glm::vec3 s {_aspectRatio.w, _aspectRatio.h, 1.0f};
	auto & m {*_root.mat_ptr()};
	m = glm::scale (glm::mat4(), s * ZOOM_FACTOR);
	m = glm::translate (m, _cameraPos);
	_aspectRatio.update = false;
}

TankView::TankView()
{
	aspect_update();
}

TankView::~TankView()
{
}

StateBase * TankView::update()
{
	pond::update_interests();
	int xcam {0};
	if (controller::is_held (SDLK_a))
		xcam += 1;
	if (controller::is_held (SDLK_d))
		xcam -= 1;
	int ycam {0};
	if (controller::is_held (SDLK_s))
		ycam += 1;
	if (controller::is_held (SDLK_w))
		ycam -= 1;

	if (xcam != 0)
	{
		const auto shift {xcam * FrameTime::get_mod()};
		_cameraPos.x += shift;
		auto & m {*_root.mat_ptr()};
		m = glm::translate (m, glm::vec3 {shift, 0.0f, 0.0f});
	}
	if (ycam !=0)
	{
		const auto shift {ycam * FrameTime::get_mod()};
		_cameraPos.y += shift;
		auto & m {*_root.mat_ptr()};
		m = glm::translate (m, glm::vec3 {0.0f, shift, 0.0f});
	}

	if (controller::mouse_click (0))
	{
		const glm::vec4 gm {controller::get_mouse(), 1.0f, 1.0f};
		const glm::vec4 ctmouse {glm::inverse (_root.get_calc()) * gm};
		pond::add_interest_point (2.0f, 999, glm::vec2 (ctmouse));
		DE_OUT ("made interest point :" << ctmouse.x << ", " << ctmouse.y);
	}
	pond::per_active_fish ([] (Fish * f) {f->update();});
	return nullptr;
}

void TankView::draw()
{
	if (_aspectRatio.update)
		aspect_update();
	_root.render();
}
