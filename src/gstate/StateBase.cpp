#include "StateBase.hpp"

StateBase::AR StateBase::_aspectRatio {false, 1.0f, 1.0f};

void StateBase::update_aspect (float w, float h)
{
	_aspectRatio = {true, w, h};
}
