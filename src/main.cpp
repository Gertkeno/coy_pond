#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#include <glm/gtc/matrix_transform.hpp>

#include <cstring>
#include <cstdlib>
#include <random>
#include <ctime>
#include <unistd.h>

#include "DebugOut.hpp"
#include "Fish.hpp"
#include "GameManager.hpp"
#include "gstate/TankView.hpp"
#include "PondManager.hpp"

bool open_assets();
void close_assets();

int main (int argc, char ** argv)
{
	/*for (int i {1}; i < argc; ++i)
	{
	}*/

	std::srand (std::time (nullptr));
	///SDL INITS
	if (SDL_Init (SDL_INIT_EVERYTHING) > 0)
	{
		DE_OUT ("SDL couldn't init\n" << SDL_GetError());
		return 1;
	}
	SDL_StopTextInput();
	SDL_GL_SetAttribute (SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute (SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute (SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute (SDL_GL_STENCIL_SIZE, 8);

	static constexpr int flags {IMG_INIT_JPG | IMG_INIT_PNG};
	if ((IMG_Init (flags)&flags) != flags)
	{
		DE_OUT ("SDL_image couldn't init\n" << IMG_GetError());
		return 3;
	}

	// TEST GARBAGE
	///////////////
	/*Node root;
	root.edit_mat (glm::scale (glm::mat4(), glm::vec3 (0.3f)));
	Fish testF (gene_t::NONE, gene_t::NONE, &root);

	glClearColor (0.1f, 0.1f, 0.0f, 1.0f);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);

	testF.update();
	root.render (glm::mat4(), false);

	SDL_GL_SwapWindow (window);*/
	///////////
	// END TEST

	// updates
	try
	{
		GameManager game (new TankView);

		if (not open_assets())
		{
			DE_OUT ("Failed to open assets\n");
			return EXIT_FAILURE;
		}
		pond::init_base();

		while (game.update());
	}
	catch (const std::exception& e)
	{
		DE_OUT ("Game Crash: " << e.what());
		SDL_ShowSimpleMessageBox (SDL_MESSAGEBOX_ERROR, "Unhandled exception", e.what(), nullptr);
	}

	close_assets();
	pond::save();
	pond::destroy();
	SDL_Quit();
	return EXIT_SUCCESS;
}
