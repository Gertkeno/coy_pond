#include "Window.hpp"
#include <SDL2/SDL_video.h>
#include "DebugOut.hpp"

static constexpr auto base_window_flags {SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE};

inline SDL_Window * initializer (const char * title, const char * settings)
{
	int s[2] {800, 600};
	unsigned flags;
	FILE * sdata {fopen (settings, "rb")};
	if (sdata != nullptr)
	{
		fread (s, sizeof (int), 2, sdata);
		fread (&flags, sizeof (flags), 1, sdata);
		fclose (sdata);
	}

	flags |= base_window_flags;
	return SDL_CreateWindow (title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, s[0], s[1], flags);
}

Window::Window (const char * title, const char * settings)
: SETTINGS_FILENAME {settings}
, ptr {initializer (title, SETTINGS_FILENAME)}
, gl {SDL_GL_CreateContext (ptr)}
{
}

Window::~Window()
{
	// save file
	auto sdata {fopen (SETTINGS_FILENAME, "wb")};
	int s[2];
	SDL_GetWindowSize (ptr, s+0, s+1);
	unsigned flags {SDL_GetWindowFlags (ptr)};
	fwrite (s, sizeof (int), 2, sdata);
	fwrite (&flags, sizeof (flags), 1, sdata);
	fclose (sdata);
	SDL_GL_DeleteContext (gl);
	SDL_DestroyWindow (ptr);
}
