#include "PondManager.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <SDL2/SDL_rwops.h>
#include <array>

#include "Fish.hpp"
#include "FrameTime.hpp"
#include "DebugOut.hpp"
#include "Node.hpp"
#include "AssetList.hpp"
#include "gstate/TankView.hpp"

namespace pond
{
namespace
{
	constexpr const char * SAVE_FILE_NAME {"tanksave.dat"};

	constexpr int MAX_TANK_SIZE {0x7F};
	std::array <Fish *, MAX_TANK_SIZE> _fishs;

	constexpr int MAX_INTREST_SIZE {0x20};
	Vertex * squareVerts {nullptr};

	bool splash_render (void * f);
	struct IntrestPoint
	{
		IntrestPoint()
		: priority {-1}
		, life {0.0f}
		, _node (squareVerts, splash_render, this)
		{
			TankView::get_root()->add_child (&_node);
		}

		int priority;
		float life;
		glm::vec2 pos;
		void update (glm::vec2 pos, float life, int pri)
		{
			this->pos = pos;
			this->life = life;
			this->priority = pri;
			auto m {_node.mat_ptr()};
			*m = glm::translate (glm::mat4(), glm::vec3 (pos, 0.0f));
			*m = glm::scale (*m, glm::vec3 (0.2f));
		}
	private:
		Node _node;
	};
	IntrestPoint * _interests;

	bool splash_render (void * f)
	{
		auto * r {static_cast <IntrestPoint *> (f)};
		if (r->life <= 0.0f)
			return false;
		gShades [shader_t::FISH].enable();
		gTexture [texture_t::KOI_2].enable();
		return true;
	}

	constexpr float INTREST_MAX_DIST {2.0f};
	constexpr float INTREST_MIN_DIST {0.55f};
	constexpr float INTREST_MIN_ANGLE {0.3f};

	constexpr float square_verts[]{
		-1.0f, -1.0f, 0.0f, 0.0f, 1.0f, //bl
		-1.0f,  1.0f, 0.0f, 0.0f, 0.0f, //tl
		 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, //br
		 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, //tr
	};

	// inventory
	std::array <unsigned, ITEM_MAX> _items;
}

void init_base()
{
	squareVerts = new Vertex {4};
	std::memcpy (squareVerts->get_vert_ptr(), square_verts, squareVerts->total_byte);
	squareVerts->sub_buffer();
	_interests = new IntrestPoint [MAX_INTREST_SIZE];
	//destroy();
}

void destroy()
{
	for (auto i {0u}; i < MAX_TANK_SIZE; ++i)
	{
		if (_fishs [i] == nullptr)
			continue;
		delete _fishs [i];
	}
	delete [] _interests;
}

void fresh_start()
{
	for (auto i {0}; i < MAX_INTREST_SIZE; ++i)
	{
		_interests [i].life = 0.0f;
	}

	for (auto & i : _fishs)
	{
		if (i == nullptr)
			continue;
		DE_OUT ("Deleting fish " << i);
		delete i;
		i = nullptr;
	}
}

void save()
{
	auto file {SDL_RWFromFile (SAVE_FILE_NAME, "wb")};
	if (file == nullptr)
	{
		DE_OUT ("Failed saving file \"" << SAVE_FILE_NAME << '"');
		return;
	}

	const auto count {fish_count()};
	SDL_RWwrite (file, &count, sizeof (count), 1);
	for (auto & i : _fishs)
	{
		if (i == nullptr)
			continue;
		SDL_RWwrite (file, i->myGenes, sizeof (gene_t), 2);
		auto vec {i->get_vec()};
		SDL_RWwrite (file, glm::value_ptr (vec), sizeof (float), 4);
		auto rand {i->get_random()};
		SDL_RWwrite (file, &rand, sizeof (rand), 1);
	}
}

bool load()
{
	auto file {SDL_RWFromFile (SAVE_FILE_NAME, "rb")};
	if (file == nullptr)
	{
		DE_OUT ("Failed loading file \"" << SAVE_FILE_NAME << '"');
		return false;
	}

	std::size_t count;
	SDL_RWread (file, &count, sizeof (count), 1);
	while (count > 0)
	{
		gene_t genes [2];
		SDL_RWread (file, genes, sizeof (gene_t), 2);
		float vec [4];
		SDL_RWread (file, vec, sizeof (float), 4);
		int rand;
		SDL_RWread (file, &rand, sizeof (int), 1);

		glm::vec4 ctVec {vec [0], vec [1], vec [2], vec [3]};
		add_fish (new Fish {genes [0], genes [1], TankView::get_root(), ctVec, rand});
		--count;
	}
	return true;
}

std::size_t fish_count()
{
	std::size_t total {0u};
	for (auto & i : _fishs)
		if (i != nullptr)
			++total;
	return total;
}

void per_active_fish (void (*f)(Fish *))
{
	for (auto & i : _fishs)
	{
		if (i == nullptr)
			continue;
		f (i);
	}
}

void add_fish (Fish * f)
{
	for (auto & i : _fishs)
	{
		if (i != nullptr)
			continue;
		i = f;
		return;
	}

	delete f;
}

void add_interest_point (float life, int priority, glm::vec2 pos)
{
	for (auto i {0}; i < MAX_INTREST_SIZE; ++i)
	{
		if (_interests [i].life > 0.0f)
			continue;
		_interests [i].update (pos, life, priority);
		break;
	}
}

void update_interests()
{
	for (auto i {0}; i < MAX_INTREST_SIZE; ++i)
	{
		if (_interests [i].life <= 0.0f)
			continue;
		_interests [i].life -= FrameTime::get_mod();
	}
}

std::pair<bool, glm::vec2> get_interest_point (const Fish * f)
{
	const auto fish {f->get_vec()};
	for (auto i {0}; i < MAX_INTREST_SIZE; ++i)
	{
		if (_interests [i].life <= 0.0f or _interests [i].priority < 0)
			continue;

		const auto dist {glm::distance (glm::vec2 (fish), _interests [i].pos)};
		if (dist > INTREST_MAX_DIST or dist < INTREST_MIN_DIST)
			continue;

		const auto dir_to_i {glm::normalize (_interests [i].pos - glm::vec2 (fish))};
		const auto dot {glm::dot (glm::vec2 (fish.z, fish.w), dir_to_i)};
		if (dot < INTREST_MIN_ANGLE)
			continue;
		_interests [i].life = 0.0f;
		return {true, _interests [i].pos};
	}
	return {false, glm::vec2()};
}

// inventory

std::array <unsigned, ITEM_MAX> & get_item_array()
{
	return _items;
}
}// pond
