#include "FrameTime.hpp"
#include <SDL2/SDL_timer.h>

namespace FrameTime
{
	namespace
	{
		float _pure;
		float _modified;
		float _modBuffer {1.0};
		unsigned _frameStart;
	}

	float get_pure()
	{
		return _pure;
	}

	float get_mod()
	{
		return _modified;
	}

	void set_mod (double set)
	{
		_modBuffer = set;
	}

	void update()
	{
		static constexpr double MS_CONVERTER {1.0/1000.0};
		_pure = since_update() * MS_CONVERTER;
		_modified = _pure * _modBuffer;
		_frameStart = SDL_GetTicks();
	}

	int since_update()
	{
		return SDL_GetTicks() - _frameStart;
	}
}
