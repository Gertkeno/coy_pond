#include "AssetList.hpp"
#include "Shader.hpp"

Texture * gTexture;
Shader * gShades;

bool open_assets()
{
	gShades = new Shader [shader_t::TOTAL] {
		{"glsl/fish.frag", "glsl/basic.vert"},
		{"glsl/debug.frag", "glsl/basic.vert"},
	};

	gTexture = new Texture [texture_t::TOTAL] {
		{"assets/Koi1.png"},
		{"assets/Koi2.png"},
		{"assets/Koi3.png"},
	};
	return true;
}

void close_assets()
{
	delete [] gTexture;
}
