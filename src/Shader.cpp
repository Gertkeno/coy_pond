#include "Shader.hpp"

#include <SDL2/SDL_rwops.h>
#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include <stdexcept>

#include <iostream>

Shader::Shader (const char * frag, const char * vert)
: _shaders {glCreateShader (GL_FRAGMENT_SHADER), glCreateShader (GL_VERTEX_SHADER)}
, _program {glCreateProgram()}
{
	const bool fragSuccess {_read_shader_file (_shaders[0], frag)};
	const bool vertSuccess {_read_shader_file (_shaders[1], vert)};

	if (not (fragSuccess or vertSuccess))
		throw std::runtime_error {"Failed to validate shaders, check logs if avaliable"};

	glAttachShader (_program, _shaders [0]);
	glAttachShader (_program, _shaders [1]);

	glLinkProgram (_program);
}

Shader::~Shader()
{
	glDeleteProgram (_program);
	glDeleteShader (_shaders [0]);
	glDeleteShader (_shaders [1]);
}

bool Shader::_read_shader_file (unsigned & shade, const char * fn)
{
	// basic file checks
	SDL_RWops * source {SDL_RWFromFile (fn, "r")};
	if (source == nullptr)
		throw std::runtime_error {"Shader file \"" + std::string (fn) + "\" could not be openend"};

	const auto size {SDL_RWsize (source)};
	if (size == 0)
		throw std::runtime_error {"Shader file \"" + std::string (fn) + "\" read size 0"};

	// buffer file
	char * buffer {new char[size]};
	SDL_RWread (source, buffer, sizeof (char), size);
	buffer [size-1] = '\0';
	SDL_RWclose (source);

	// gl make shader
	glShaderSource (shade, 1, &buffer, nullptr);
	delete [] buffer;
	glCompileShader (shade);

	// check compile status
	GLint status;
	glGetShaderiv (shade, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE)
	{
		std::cerr << "Error making shader " << fn << '\n';
		GLsizei logLength {0};
		glGetShaderiv (shade, GL_INFO_LOG_LENGTH, &logLength);
		char * err {new char [logLength]};
		glGetShaderInfoLog (shade, logLength, nullptr, err);
		std::cerr << err << std::endl;
		delete [] err;
		return false;
	}
	return true;
}

void Shader::enable() const
{
	glUseProgram (_program);
}

void Shader::uniform (int id, float val)
{
	glUniform1f (id, val);
}

void Shader::uniform (int id, glm::vec2 val)
{
	glUniform2fv (id, 1, glm::value_ptr (val));
}

void Shader::uniform (int id, glm::vec3 val)
{
	glUniform3fv (id, 1, glm::value_ptr (val));
}

void Shader::uniform (int id, glm::mat4 val)
{
	glUniformMatrix4fv (id, 1, GL_FALSE, glm::value_ptr (val));
}
