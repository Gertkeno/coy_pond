add_library (Game Vertex.cpp Fish.cpp GameManager.cpp Texture.cpp FrameTime.cpp gstate gstate/TankView.cpp gstate/StateBase.cpp Controller.cpp Window.cpp PondManager.cpp Shader.cpp AssetOpen.cpp Node.cpp)

find_package (SDL2 REQUIRED)
find_package (glm)
find_package (GLEW REQUIRED)
find_package (OpenGL REQUIRED)

add_executable (coy_pond main.cpp)
target_link_libraries (coy_pond ${SDL2_LIBRARIES} ${GLEW_LIBRARIES} ${OPENGL_LIBRARIES} SDL2_image Game)
SET (CMAKE_CXX_FLAGS_DEBUG "-Wall -Wextra -O0 -g")
SET (CMAKE_CXX_FLAGS_RELEASE "-O3")
