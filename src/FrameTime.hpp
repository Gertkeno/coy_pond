#ifndef FRAME_TIME_H
#define FRAME_TIME_H

namespace FrameTime
{
	float get_pure();
	float get_mod();
	void set_mod( double set );

	void update();
	int since_update();
}

#endif //FRAME_TIME_H
