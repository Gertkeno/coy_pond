#include "GameManager.hpp"

#include <cassert>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <stdexcept>

#include "FrameTime.hpp"
#include "gstate/StateBase.hpp"
#include "AssetList.hpp"
#include "Controller.hpp"

#ifndef NDEBUG
#include <iostream>
#include "PondManager.hpp"
#include "Fish.hpp"
#include "gstate/TankView.hpp"
#endif

static constexpr auto base_window_flags {SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE};

GameManager::GameManager(StateBase * startIn)
: _state {startIn}
, _window {"Coy Pond", "wsettings.dat"}
, _event {new SDL_Event}
{
	assert (startIn != nullptr);

	glewExperimental = GL_TRUE;
	const auto glewInitCode {glewInit()};
	if (glewInitCode != GLEW_OK)
		throw std::runtime_error {"Glew couldn't initialize\n"};

	glEnable (GL_DEPTH_TEST);
	_give_window();
}

GameManager::~GameManager()
{
	delete _event;
}

bool GameManager::update()
{
	FrameTime::update();
	controller::update();
	while (SDL_PollEvent (_event))
	{
		switch (_event->type)
		{
		case SDL_KEYDOWN:
			if (_event->key.keysym.sym == SDLK_q and (SDL_GetModState() & KMOD_CTRL) != 0)
				return false;
#ifndef NDEBUG
			else if (_event->key.keysym.sym == SDLK_F1)
				pond::add_fish (new Fish (gene_t::RAINBOW, gene_t::RAINBOW, TankView::get_root()));
			else if (_event->key.keysym.sym == SDLK_F2)
				pond::add_fish (new Fish (gene_t::LONG, gene_t::NONE, TankView::get_root()));
			else if (_event->key.keysym.sym == SDLK_F3)
				pond::fresh_start();
			else if (_event->key.keysym.sym == SDLK_F12)
				pond::load();
#endif
			break;
		case SDL_QUIT:
			return false;
		case SDL_WINDOWEVENT:
			if (_event->window.event == SDL_WINDOWEVENT_RESIZED)
				_give_window();
			break;
		}
		controller::events (_event);
	}

	const auto nstate {_state->update()};
	if (nstate != nullptr)
	{
		delete _state;
		_state = nstate;
	}

	// drawing
	glClearColor (0.6f, 0.6f, 0.0f, 1.0f);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	_state->draw();

	SDL_GL_SwapWindow (_window.ptr);

	static constexpr int MAX_FPS {1000/60};
	SDL_Delay (std::max (0, MAX_FPS - FrameTime::since_update()));
	return true;
}

void GameManager::_give_window() const
{
	int w, h;
	SDL_GetWindowSize (_window.ptr, &w, &h);
	glViewport (0, 0, w, h);
	controller::update_window (w, h);
	const float max (std::max (w,h));
	StateBase::update_aspect (h/max, w/max);
}
