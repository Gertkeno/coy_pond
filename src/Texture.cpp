#include "Texture.hpp"

#include <SDL2/SDL_image.h>
#include <GL/glew.h>
#include <cassert>
#include <stdexcept>

#include "DebugOut.hpp"

Texture::Texture (const char * fn)
{
	assert (fn != nullptr);
	glGenTextures (1, &_textureIndex);

	const auto surf {IMG_Load (fn)};
	if (surf == nullptr)
		throw std::runtime_error {"Couldn't open texture: \"" + std::string (fn) + "\": " + std::string (IMG_GetError())};

	glBindTexture (GL_TEXTURE_2D, _textureIndex);

	const int ctMode {surf->format->BytesPerPixel == 3 ? GL_RGB : GL_RGBA};

	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB8, surf->w, surf->h, 0, ctMode, GL_UNSIGNED_BYTE, surf->pixels);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	SDL_FreeSurface (surf);
	glBindTexture (GL_TEXTURE_2D, 0);
}

Texture::~Texture()
{
	glDeleteTextures (1, &_textureIndex);
}

void Texture::enable (int texNum) const
{
	glActiveTexture (GL_TEXTURE0 + texNum);
	glBindTexture (GL_TEXTURE_2D, _textureIndex);

	glUniform1i (20+texNum, texNum);
}
