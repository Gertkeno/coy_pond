#include "Node.hpp"

#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>

#include "Shader.hpp"
#include "Texture.hpp"
#include "Vertex.hpp"

#ifndef NDEBUG
#include "DebugOut.hpp"
#include "AssetList.hpp"
#endif

namespace
{
	constexpr int MODEL_LOCATION {5};
	constexpr int COLOR_LOCATION {6};
}

Node::Node (Vertex * ve, node_render rf, void * pf)
: _dirty {true}
, _renderData {pf}
, renderFunc {rf}
, myVerts {ve}
, mySlice {0.0f, 0.0f, 1.0f, 1.0f}
, myColor {1.0f, 1.0f, 1.0f}
{}

Node::~Node()
{}

void Node::render (const glm::mat4& parent, bool dirty)
{
	dirty = dirty or _dirty;
	if (dirty)
	{
		_calc = parent * _mat;// * parent;
		_dirty = false;
	}

	_draw();

	for (auto & i : _children)
		i->render (_calc, dirty);
}

glm::vec3 Node::position() const
{
	return {_mat[3][0], _mat[3][1], _mat[3][2]};
}

Node * Node::add_child (Node * c)
{
	assert (c != nullptr);
	_children.push_back (c);
	//DE_OUT (this << " added child " << c);
	return c;
}

bool Node::remove_child (Node * c)
{
	assert (c != nullptr);
	for (auto i = _children.begin(); i != _children.end(); ++i)
	{
		if (c != *i)
			continue;
		_children.erase (i);
		return true;
	}
	return false;
}

void Node::_draw() const
{
	if (myVerts == nullptr)
		return;

	if (renderFunc != nullptr)
		if (not renderFunc (_renderData))
			return;
	glUniformMatrix4fv (MODEL_LOCATION, 1, GL_FALSE, glm::value_ptr (_calc));
	glUniform3fv (COLOR_LOCATION, 1, glm::value_ptr (myColor));
	myVerts->draw();

#ifndef NDEBUG
	gShades [shader_t::DEBUG].enable();
	glUniformMatrix4fv (MODEL_LOCATION, 1, GL_FALSE, glm::value_ptr (_calc));
	myVerts->draw();
#endif
}

glm::mat4 Node::get_calc() const
{
	return _calc;
}

glm::mat4 Node::get_relative() const
{
	return _mat;
}

void Node::edit_mat (const glm::mat4 & m)
{
	_dirty = true;
	_mat = m;
}

glm::mat4 * Node::mat_ptr()
{
	_dirty = true;
	return & _mat;
}

