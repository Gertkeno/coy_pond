#pragma once

#include <glm/glm.hpp>
#include <vector>

class Vertex;

using node_render = bool (*) (void *);

class Node
{
private:
	std::vector<Node *> _children;
	glm::mat4 _mat;
	glm::mat4 _calc;
	bool _dirty;
	void * _renderData;
protected:
	virtual void _draw() const;
public:
	Node (Vertex *, node_render, void * pf);
	Node (Vertex * v = nullptr): Node (v, nullptr, nullptr) {}
	virtual ~Node();

	node_render renderFunc;
	const Vertex * myVerts;
	glm::vec4 mySlice;
	glm::vec3 myColor;

	void edit_mat (const glm::mat4 &);
	glm::mat4 * mat_ptr();
	glm::mat4 get_relative() const;
	glm::mat4 get_calc() const;

	void render (const glm::mat4& = glm::mat4(), bool = false);
	glm::vec3 position() const;
	Node * add_child (Node *);
	bool remove_child (Node *);
};
