#pragma once
#include <cstddef>

class Vertex
{
	unsigned _arrayObject, _vertexArray;
	float * _verts;
public:
	const std::size_t total;
	const std::size_t total_byte;
	Vertex (std::size_t total);
	~Vertex();

	void sub_buffer();

	float * get_vert_ptr();

	void draw() const;
private:
};
