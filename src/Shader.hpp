#pragma once
#include <glm/glm.hpp>

class Shader
{
	// fragment first, vertex second
	unsigned _shaders [2];
	unsigned _program;

	bool _read_shader_file (unsigned & shade, const char * fn);
public:
	// filename
	Shader (const char * frag, const char * vert);
	~Shader();

	void enable() const;
	static void uniform (int id, float val);
	static void uniform (int id, glm::vec2 val);
	static void uniform (int id, glm::vec3 val);
	static void uniform (int id, glm::mat4 val);
};
