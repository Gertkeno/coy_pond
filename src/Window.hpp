#pragma once
class SDL_Window;
using SDL_GLContext = void *;

class Window
{
	const char * const SETTINGS_FILENAME;
public:
	SDL_Window * const ptr;
	SDL_GLContext const gl;

	Window (const char * title, const char * settings);
	~Window();
};
