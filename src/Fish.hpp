#pragma once
#include <glm/glm.hpp>
#include <utility>

#include "Genome.hpp"
#include "Node.hpp"
#include "Vertex.hpp"

class Texture;

class Node;

class Fish
{
public:
	const gene_t myGenes[2];
	// loaded fish
	Fish (gene_t parentA, gene_t parentB, Node *, glm::vec4 pos, int rand);
	// natural fish
	Fish (gene_t parentA, gene_t parentB, Node *);
	~Fish();

	void update();

	// either gene contains 'c'
	bool or_gene (gene_t c) const {return ((myGenes [0] | myGenes [1]) & c) != 0;}
	// both genes contain 'c'
	bool and_gene (gene_t c) const {return (myGenes [0] & c) != 0 and (myGenes [1] & c) != 0;}
	// exactly one gene contains 'c'
	bool xor_gene (gene_t c) const {return or_gene (c) and not and_gene (c);}

	// x, y position; z, w direction;
	glm::vec4 get_vec() const {return {_segments [0], _headDirection};}
	void set_vec (glm::vec4);

	int get_random() const {return _randomSeed;}
	float get_rainbow_adjust() const;
	int get_random_range (int start, int count) const;
private:
	const unsigned char _segmentTotal;
	glm::vec2 * _segments;
	glm::vec2 _headDirection;// normalize please

	std::pair <bool, glm::vec2> _interest;
	float _speed;
	float _lookTimer;
	int _interestBoredom;

	Vertex _verts;
	Node _position;
	Node * const _parentNode;

	int _randomSeed;

	bool _look_for_interest();
	void _update_verticies();
	bool _move_forward();
};
