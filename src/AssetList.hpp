#pragma once
#include "Texture.hpp"
#include "Shader.hpp"

namespace texture_t
{
	enum
	{
		KOI_0,
		KOI_1,
		KOI_2,
		TOTAL,
	};

	constexpr auto KOI_START {KOI_0};
	constexpr auto KOI_TOTAL {KOI_2 - KOI_0 + 1};
}

extern Texture * gTexture;

namespace shader_t
{
	enum
	{
		FISH,
		DEBUG,
		TOTAL,
	};
}

extern Shader * gShades;
