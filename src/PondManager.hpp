#pragma once
#include <glm/glm.hpp>
#include <utility>
#include <array>
class Fish;

namespace pond
{
	void init_base();
	void destroy();
	void fresh_start();
	void save();
	bool load();
	std::size_t fish_count();

	// always give new Fish()
	void add_fish (Fish *);

	void per_active_fish (void (*f)(Fish *));

	void add_interest_point (float life, int priority, glm::vec2 pos);
	void update_interests();
	std::pair <bool, glm::vec2> get_interest_point (const Fish *);

	static constexpr auto ITEM_MAX {50};
	std::array <unsigned, ITEM_MAX> & get_item_array();
}
