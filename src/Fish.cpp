#include "Fish.hpp"

#include <SDL2/SDL_timer.h>
#include <cstring>
#include <random>

#include "DebugOut.hpp"
#include "AssetList.hpp"
#include "FrameTime.hpp"
#include "PondManager.hpp"

inline float rand_range (const float max, const float min=0)
{
	return static_cast <float> (std::rand()) / RAND_MAX * (max - min)  + min;
}

constexpr float rand_to_tao (int rand)
{
	return rand * (1.0f / (RAND_MAX * M_PI * 2));
}

namespace
{
	// vertex creation
	constexpr auto VERT_BASE_SEGMENTS {2};
	constexpr auto VERT_PER_SEGMENT {2};
	constexpr auto SEGMENT_WIDTH {0.7f};
	constexpr auto SEGMENT_HEIGHT {0.65f}; // should be a length of each segment
	constexpr float Z_DEPTH {0.0f};
	constexpr float HEAD_HEIGHT {0.4f};

	// uv points, make this variable later
	constexpr float NECK_TOP {0.78125f};
	constexpr float BODY_TOP {0.4375};

	// game thresholds
	constexpr float INTREST_RADIUS {0.25f};
	constexpr float MAX_SPEED {0.4f};
	constexpr float INTEREST_TIMER {1.4f};
	constexpr float ACCELERATION {0.28f};
	constexpr auto BOREDOM_MAX {4};

	// draw tools
	constexpr float SWAY_SPEED {2.6f};
	constexpr float SWAY_MAGNITUDE {0.036f};

	bool basic_fish (void * dat)
	{
		Fish * r (static_cast <Fish *> (dat));
		gShades [shader_t::FISH].enable();
		Shader::uniform (7, r->get_rainbow_adjust());
		gTexture [r->get_random_range (texture_t::KOI_START, texture_t::KOI_TOTAL)].enable();
		return true;
	}
}

Fish::Fish (gene_t a, gene_t b, Node * node, glm::vec4 pos, int rand)
// genes
: myGenes {a, b}

// segments
, _segmentTotal (xor_gene (gene_t::LONG) ? 5 : 3)
, _segments (new glm::vec2 [_segmentTotal])
, _headDirection (pos.z, pos.w)

// movement
, _interest {false, {0.0f, 0.0f}}
, _speed {0.04f}
, _lookTimer {0.0f}
, _interestBoredom {0}

// draw
, _verts (VERT_PER_SEGMENT*_segmentTotal + VERT_BASE_SEGMENTS)
, _position {&_verts, basic_fish, this}
, _parentNode {node}
, _randomSeed {rand}
{
	_parentNode->add_child (&_position);
	auto & head {_segments [0]};
	head = glm::vec2 (pos);
	for (auto i {1u}; i < _segmentTotal; ++i)
	{
		_segments [i] = {head.x + i * -_headDirection.x * SEGMENT_HEIGHT, head.y + i * -_headDirection.y * SEGMENT_HEIGHT};
	}
}

Fish::Fish (gene_t a, gene_t b, Node * n)
: Fish (a, b, n, glm::vec4 {0.0f, 0.0f, 1.0f, 0.0f}, std::rand())
{
}

Fish::~Fish()
{
	_parentNode->remove_child (&_position);
	delete [] _segments;
}

void Fish::update()
{
	_lookTimer += FrameTime::get_mod();

	if (_move_forward())
		_update_verticies();
	else
		_look_for_interest();
}

bool Fish::_move_forward() // return if moved
{
	if (_interest.first)
	{
		const auto dist {glm::distance (_segments [0], _interest.second)};
		if (dist < INTREST_RADIUS)
			_interest.first = false;

		if (_speed < MAX_SPEED)
		{
			_speed += ACCELERATION * FrameTime::get_mod();
			_speed = std::min (_speed, MAX_SPEED);
		}
	}

	if (_speed <= 0.0f)
		return false;

	_segments [0] += _headDirection * _speed * FrameTime::get_mod();

	for (auto i {1u}; i < _segmentTotal; ++i)
	{
		auto & s {_segments [i]};
		const auto & last {_segments [i-1]};

		const auto swayVector {[&]() -> glm::vec2
		{
			const auto dir {glm::normalize (last - s)};
			if (i & 1)
				return {dir.y, -dir.x};
			return {-dir.y, dir.x};
		}()};
		s += swayVector * std::sin (_lookTimer * SWAY_SPEED) * SWAY_MAGNITUDE * _speed/MAX_SPEED * FrameTime::get_mod();

		const auto dist {glm::distance (s, last)};
		if (dist <= SEGMENT_HEIGHT)
			continue;
		s += glm::normalize (last - s) * (dist - SEGMENT_HEIGHT);
	}

	if (not _interest.first)
	{
		_speed -= MAX_SPEED * FrameTime::get_mod();
		if (_speed < 0.0f)
		{
			_speed = 0.0f;
			_lookTimer = 0.0f;
		}
	}
	return true;
}

void Fish::_update_verticies()
{
	auto ut {_verts.get_vert_ptr()};
	// lambda to make a lot of this crap nicer

	auto set_vert {[&] (glm::vec2 v, glm::vec2 t)
	{
		*(ut++) = v.x;
		*(ut++) = v.y;
		*(ut++) = Z_DEPTH;

		*(ut++) = t.x;
		*(ut++) = t.y;
	}};

	auto set_vert_complete {[&] (glm::vec2 base, glm::vec2 dir, float texy)
	{
		const glm::vec2 cw {dir.y, -dir.x};
		const glm::vec2 ccw {-dir.y, dir.x};

		set_vert (ccw * SEGMENT_WIDTH + base, {0.0f, texy});
		set_vert (cw * SEGMENT_WIDTH + base, {1.0f, texy});
	}};

	// top of the head
	set_vert_complete (_segments [0] + _headDirection * HEAD_HEIGHT, _headDirection, 1.0f);
	// neck
	set_vert_complete (_segments [0], _headDirection, NECK_TOP);

	for (unsigned char i {1u}; i < _segmentTotal; ++i)
	{
		const auto & s {_segments [i]};
		auto dir {glm::normalize (_segments [i-1] - s)};
		if (std::isnan (dir.x) or std::isnan (dir.y))
			dir = {0.0f, 0.0f};

		const float tailTexy {i == _segmentTotal-1 ? 0.0f : (i & 1 ? BODY_TOP : NECK_TOP)};
		set_vert_complete (s, dir, tailTexy);
	}
	_verts.sub_buffer();
}

bool Fish::_look_for_interest() // return if found interest
{
	if (_lookTimer < INTEREST_TIMER)
		return false;

	if (_interestBoredom++ > BOREDOM_MAX)
	{
		const glm::vec2 randVec {rand_range (0.7f, -0.7f), rand_range (0.7f, -0.7f)};
		_interest = {true, _segments [0] + _headDirection + randVec};
		//std::cout << "Bored, going to " << _interest.second.x << ", " << _interest.second.y << '\n';
	}
	else
	{
		_interest = pond::get_interest_point (this);
	}

	if (_interest.first)
	{
		_interestBoredom = std::rand() % BOREDOM_MAX;
		_headDirection = glm::normalize (_interest.second - _segments [0]);
	}
	_lookTimer = 0.0f;
	return _interest.first;
}

void Fish::set_vec (glm::vec4 p)
{
	_segments [0] = glm::vec2 (p.x, p.y);
	_headDirection = glm::normalize (glm::vec2 (p.z, p.w));
}

float Fish::get_rainbow_adjust() const
{
	if (and_gene (gene_t::RAINBOW))
		return (SDL_GetTicks() + _randomSeed)/1000.0f;
	return rand_to_tao (_randomSeed);
}

int Fish::get_random_range (int start, int count) const
{
	return _randomSeed % count + start;
}
