#include "Controller.hpp"
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_events.h>
#include <map>

#ifndef NDEBUG
#include <iostream>
#endif

namespace controller
{
namespace
{
	enum : char
	{
		PRESSED,
		HELD,
		RELEASED,
	};
	std::map <SDL_Keycode, char> _keys;

	struct
	{
		float w, h;
	} _windowSize;

	glm::vec2 _mouse;
	// 0: left 1: right
	enum click_t
	{
		LEFT,
		RIGHT,
		TOTAL
	};
	char _clicked [click_t::TOTAL] {RELEASED, RELEASED};
}

void update()
{
	for (auto & i : _keys)
		if (i.second == PRESSED)
			i.second = HELD;

	for (auto & i : _clicked)
		if (i == PRESSED)
			i = HELD;
}

inline int mbget (int b)
{
	if (b == SDL_BUTTON_LEFT)
		return LEFT;
	else if (b == SDL_BUTTON_RIGHT)
		return RIGHT;
	return TOTAL;
}

inline float mouse_math (int pos, float wh)
{
	return (pos / wh) * 2 - 1;
}

void events (SDL_Event * e)
{
	switch (e->type)
	{
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		if (_keys.count (e->key.keysym.sym) < 1)
			break;
		_keys [e->key.keysym.sym] = e->key.state == SDL_PRESSED ? PRESSED : RELEASED;
		break;
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP: {
		const auto mbg {mbget (e->button.button)};
		if (mbg == TOTAL)
			break;
		_clicked [mbg] = e->button.state == SDL_PRESSED ? PRESSED : RELEASED;
		}break;
	case SDL_MOUSEMOTION:
		_mouse = {mouse_math (e->motion.x, _windowSize.w), -mouse_math (e->motion.y, _windowSize.h)};
		break;
	}
}

glm::vec2 get_mouse()
{
	return _mouse;
}

void update_window (int w, int h)
{
	_windowSize.w = w;
	_windowSize.h = h;
}

bool is_pressed (int keycode)
{
	if (_keys.count (keycode) < 1)
		_keys [keycode] = RELEASED;
	return _keys.at (keycode) == PRESSED;
}

bool is_held (int keycode)
{
	if (_keys.count (keycode) < 1)
		_keys [keycode] = RELEASED;
	return _keys.at (keycode) < RELEASED;
}

bool mouse_click(int right)
{
	return _clicked [right] == PRESSED;
}

bool mouse_held (int right)
{
	return _clicked [right] < RELEASED;
}
}// controller
