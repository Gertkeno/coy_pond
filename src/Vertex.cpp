#include "Vertex.hpp"
#include <GL/glew.h>
#include "DebugOut.hpp"

namespace
{
	constexpr auto vert_width {5};
}

Vertex::Vertex (std::size_t t)
: total {t}
, total_byte {total * sizeof (float) * vert_width}
{
	_verts = new float [total*vert_width];
	glGenVertexArrays (1, &_arrayObject);
	glGenBuffers (1, &_vertexArray);

	// vertex array describes how and what data to use, like a pointer or containing class
	glBindVertexArray (_arrayObject);
	glBindBuffer (GL_ARRAY_BUFFER, _vertexArray);
	glBufferData (GL_ARRAY_BUFFER, total_byte, _verts, GL_DYNAMIC_DRAW);

	//position vertices
	glEnableVertexAttribArray (0);
	const auto stride {sizeof (float)*vert_width};
	glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, stride, nullptr);
	//texture UV
	glEnableVertexAttribArray (1);
	glVertexAttribPointer (1, 2, GL_FLOAT, GL_FALSE, stride, (void*)(3*sizeof (float)));
}

Vertex::~Vertex()
{
	glDeleteBuffers (1, &_vertexArray);
	glDeleteVertexArrays (1, &_arrayObject);

	delete [] _verts;
}

void Vertex::sub_buffer()
{
	/*for (auto i {0u}; i < total*vert_width; ++i)
	{
		if (i % vert_width == 0)
			DE_OUT (' ');
		DE_OUT (_verts [i] << '\t' << i % vert_width);
	}*/

	glBindVertexArray (_arrayObject);
	glBindBuffer (GL_ARRAY_BUFFER, _vertexArray);
	// http://docs.gl/gl3/glBufferSubData
	// remember to buffer size based on bytes
	glBufferSubData (GL_ARRAY_BUFFER, 0, total_byte, _verts);
	glBindVertexArray (0);
}

void Vertex::draw() const
{
	glBindVertexArray (_arrayObject);

	// draw based on object count, object size defined with vertex attribs. should match variable 'total'
	glDrawArrays (GL_TRIANGLE_STRIP, 0, total);

	glBindVertexArray (0);
}

float * Vertex::get_vert_ptr()
{
	return _verts;
}
